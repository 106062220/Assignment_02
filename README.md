# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description
# How to play :
player1:上下左右、space攻擊、z大招
player2:WASD、shift攻擊 
# Basic Components Description : 
1. Jucify mechanisms : 總共有二十關，每五關中間會有一隻Boss，小兵與Boss皆會隨著關卡而漸漸提升難度。當藍色的能量條滿的時候可以使用火球必殺技。
2. Animations : 在player方面上下左右有四種不同的動畫，被打到的時候會進入一段無敵時間(會閃爍)，小怪、Boss、player被打到時都會短暫的變成紅色。
3. Particle Systems : 無論是小幫手、player、小兵、Boss發射子彈時都會有Particle Systems。
4. Sound effects : 當進入玩遊戲頁面時，會播放BGM、發射子彈也會有射擊音效。
5. Leaderboard : 結合firebase，並將分數結果經過sort後顯示在結算頁面，一二三名用金銀銅三色去顯示。
6. UL : 左下角有用紅色及藍色分別顯示血條及能量條，血條旁邊有顯示分數及音量控制，如果點擊"-"、"+"去控制音量大小("-"比較小有點難點)，右上角有遊戲暫停及繼續的功能。
7. Appearence : menu及結算頁面背景使用星星閃爍，場景切換用黑屏去淡出、淡入。
# Bonus Functions Description : 
1. Multi-player(off-line) : 在一開始的menu即可選擇要幾個人進行遊戲，選擇雙人遊戲的話，會多出一個player，但只有射擊子彈的功能而已沒有必殺技。
2. Enhanced items : 當玩家擊殺怪物或Boss時可以得到一些分數，撿到隨機生成的前也可以得到一些分數，而分數越高角色的能力也會隨之增強。當玩家100分的時候子彈會變成散彈而且能一次射更多發。當玩家300分時會召喚出一個小幫手，當玩家500分時會再召喚出另一個小幫手，當玩家800分的時候會開啟自動瞄準模式而且會連續射更多發子彈。
3. Boss : 總共有4隻Boss，四隻攻擊模式都是一樣，一開始是隨機角度的射擊，再來顏色會變紅色，會進到一個緩衝的階段，再來會進到一種持續射擊的模式。第三隻與第四隻Boss會左右移動(cubic的tween)，第四隻會比前三隻又多一發子彈(一到三關兩發，第四關三發)。
