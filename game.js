var playState = {
    preload: function() {

        // Loat game sprites.
        game.load.image('background', 'assets/background.jpg');
        game.load.image('universe','assets/universe.jpg');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet1','assets/bullet.png');
        game.load.image('enemy1_bullet','assets/enemy1_bullet.png');
        game.load.image('coin', 'assets/coin.png');
        game.load.image('enemy1','assets/enemy1.png');
        game.load.image('heart','assets/heart.png');
        game.load.image('black','assets/Black.jpg');
        game.load.image('boss1','assets/Boss1.png');
        game.load.image('boss2','assets/Boss2.png');
        game.load.image('boss3','assets/Boss3.png');
        game.load.image('boss4','assets/Boss4.png');
        game.load.image('enemy1_1','assets/enemy1_1.png');
        game.load.image('enemy1_2','assets/enemy1_2.png');
        game.load.image('enemy1_3','assets/enemy1_3.png');
        game.load.image('enemy1_4','assets/enemy1_4.png');
        game.load.image('enemy1_5','assets/enemy1_5.png');
        game.load.image('volume_image','assets/volume.png');
        game.load.image('pause','assets/pause.png');
        game.load.image('helper','assets/helper.png');
        game.load.image('bullet_helper','assets/bullet_helper.png');
        game.load.image('aircraft','assets/aircraft.png');
        game.load.image('small_aircraft','assets/small_aircraft.png');


        game.load.spritesheet('player', 'assets/MARIO.png', 32,54);
        game.load.spritesheet('airplane','assets/plane.png',189,184);
        game.load.spritesheet('small_airplane','assets/plane.png',189,184);
        game.load.spritesheet('boom','assets/explosion.png',64,64);
        game.load.spritesheet('boom_big','assets/explosion_big.png',161,161);
        game.load.spritesheet('fireball','assets/Fireball.png',133,134);

        game.load.audio('shot1', 'assets/audio/shot1.mp3');
        game.load.audio('bgm', 'assets/audio/bgm.mp3');
    },
    create: function() {
        game.stage.backgroundColor = '#000000';
        this.back = game.add.image(-443, -82, 'universe'); 
        var margin = 30;
       // and set the world's bounds according to the given margin
        var x = -margin;
        var y = -margin;
        var w = game.world.width + margin * 2;
        var h = game.world.height + margin * 2;
        // it's not necessary to increase height, we do it to keep uniformity
        game.world.setBounds(x, y, w, h);
        game.world.camera.position.set(0);

        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)
        game.physics.startSystem(Phaser.Physics.ARCADE);
        ///
        this.shot1 = game.add.audio('shot1');
        this.shot1.allowMultiple = true;
        
        this.bgm = game.add.audio('bgm');
        this.bgm.allowMultiple = true;
        this.bgm.play();
        
        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        this.fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        this.powerButton = this.input.keyboard.addKey(Phaser.KeyCode.Z);
        this.A_Button = this.input.keyboard.addKey(Phaser.KeyCode.A);
        this.W_Button = this.input.keyboard.addKey(Phaser.KeyCode.W);
        this.S_Button = this.input.keyboard.addKey(Phaser.KeyCode.S);
        this.D_Button = this.input.keyboard.addKey(Phaser.KeyCode.D);
        this.SHIFT_Button = this.input.keyboard.addKey(Phaser.KeyCode.SHIFT);

        this.bullet1 = game.add.weapon(30,'bullet1');
        this.bullet1.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet1.bulletAngleOffset = 90;
        this.bullet1.bulletSpeed = 400;
        this.bullet1.fireRate = 180;

        this.bullet2 = game.add.weapon(50,'bullet1');
        this.bullet2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet2.bulletAngleOffset = 90;
        this.bullet2.bulletSpeed = 400;
        this.bullet2.fireRate = 100;
        this.bullet2.bulletAngleVariance = 10;

        this.bullet3 = game.add.weapon(30,'bullet_helper');
        this.bullet3.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet3.bulletAngleOffset = 90;
        this.bullet3.bulletSpeed = 400;
        this.bullet3.fireRate = 180;

        this.bullet4 = game.add.weapon(30,'bullet_helper');
        this.bullet4.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet4.bulletAngleOffset = 90;
        this.bullet4.bulletSpeed = 400;
        this.bullet4.fireRate = 180;

        this.bullet5 = game.add.weapon(30,'bullet1');
        this.bullet5.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bullet5.bulletAngleOffset = 90;
        this.bullet5.bulletSpeed = 400;
        this.bullet5.fireRate = 180;

        this.enemy1_Bullets = game.add.group();
        this.enemy1_Bullets.enableBody = true;
        this.enemy1_Bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemy1_Bullets.createMultiple(30, 'enemy1_bullet');
        this.enemy1_Bullets.setAll('anchor.x', 0.5);
        this.enemy1_Bullets.setAll('anchor.y', 1);
        this.enemy1_Bullets.setAll('outOfBoundsKill', true);
        this.enemy1_Bullets.setAll('checkWorldBounds', true);

        this.autoaim_Bullets = game.add.group();
        this.autoaim_Bullets.enableBody = true;
        this.autoaim_Bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.autoaim_Bullets.createMultiple(30, 'bullet1');
        this.autoaim_Bullets.setAll('anchor.x', 0.5);
        this.autoaim_Bullets.setAll('anchor.y', 1);
        this.autoaim_Bullets.setAll('outOfBoundsKill', true);
        this.autoaim_Bullets.setAll('checkWorldBounds', true);

        this.boss1_bullet_1 = game.add.weapon(30,'enemy1_bullet');
        this.boss1_bullet_2 = game.add.weapon(30,'enemy1_bullet');
        this.boss1_bullet_3 = game.add.weapon(30,'enemy1_bullet');

        this.small_airplane = game.add.sprite(30,game.height-30,'small_airplane');
        this.small_airplane.scale.setTo(0.2, 0.2);
        this.small_airplane.anchor.setTo(0.5,0.5);
        this.small_airplane.animations.add('normal',[12],50,false);
        this.small_airplane.animations.play('normal');

        if(player_number == 2){
            this.small_aircraft = game.add.sprite(30,game.height-110,'small_aircraft');
        this.small_aircraft.scale.setTo(0.2, 0.2);
        this.small_aircraft.anchor.setTo(0.5,0.5);
        }
        

        this.airplane = game.add.sprite(game.width/2,game.height/2+200,'airplane');
        this.airplane.scale.setTo(0.4, 0.4);
        this.airplane.anchor.setTo(0.5,0.5);
        this.airplane.animations.add('right',[13],50,false);
        this.airplane.animations.add('left',[11],50,false);
        this.airplane.animations.add('forward',[7],50,false);
        this.airplane.animations.add('backward',[17],50,false);
        this.airplane.animations.add('normal',[12],50,false);
        this.airplane.animations.add('explosion',);

        this.helper = game.add.sprite(game.width/2-70,game.height/2+200,'helper');
        this.helper.scale.setTo(0.13, 0.13);
        this.helper.anchor.setTo(0.5,0.5);
        this.helper.alpha = 0;

        this.helper_ = game.add.sprite(game.width/2+70,game.height/2+200,'helper');
        this.helper_.scale.setTo(0.13, 0.13);
        this.helper_.anchor.setTo(0.5,0.5);
        this.helper_.alpha = 0;

        this.aircraft = game.add.sprite(game.width/2-70,game.height/2+200,'aircraft');
        this.aircraft.scale.setTo(0.12, 0.12);
        this.aircraft.anchor.setTo(0.5,0.5);
        
        
        this.fireball = game.add.sprite(game.width/2,game.height/2,'fireball');
        this.fireball.scale.setTo(1, 1);
        this.fireball.alpha = 0;
        this.fireball.anchor.setTo(0.5,0.5);
        this.fireball.animations.add('burn',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],50,true);
        this.fireball.animations.play('burn');

        this.coin = game.add.sprite(game.width/2-200,game.height/2-200,'coin');
        this.coin.anchor.setTo(0.5,0.5);
        this.coin.scale.setTo(0.04,0.04);
        this.coin.alpha = 0;
        game.add.tween(this.coin).to( { alpha: 1 }, 2000, "Linear", true);

        this.heart = game.add.sprite(game.width/2+200,game.height/2+50,'heart');
        this.heart.anchor.setTo(0.5,0.5);
        this.heart.scale.setTo(0.25,0.25);
        this.heart.alpha = 0;
        this.heart_show = game.add.tween(this.heart);
        this.heart_show.to( { alpha: 1 }, 2000, "Linear", true);
        

        this.bullet1.trackSprite(this.airplane, 0, -20);
        this.bullet2.trackSprite(this.airplane, 0, -20);
        this.bullet3.trackSprite(this.helper,0,-20);
        this.bullet4.trackSprite(this.helper_,0,-20);
        this.bullet5.trackSprite(this.aircraft,0,-20);
        
        this.boss1 = game.add.sprite(game.width/2,-200,'boss1');
        this.boss1.scale.setTo(0.5, 0.5);
        this.boss1.anchor.setTo(0.5,0.5);
        this.boss1.blood = 2000;
        this.boss1.enableBody = true;
        game.physics.arcade.enable(this.boss1);
        this.boss1.physicsBodyType = Phaser.Physics.ARCADE;

        this.explosions = game.add.group();
        this.explosions.createMultiple(100, 'boom');
        this.explosions.forEach(this.setupInvader, this);

        this.explosions_big = game.add.group();
        this.explosions_big.createMultiple(100, 'boom_big');
        this.explosions_big.forEach(this.setupInvader_, this);

        
        this.scoreLabel = game.add.text(game.width/2-50, game.height-90, 'score: 0', { font: '48px Arial', fill: '#ffffff' }); // Initialize the score variable 
        
        
        this.airplane.blood = 200;
        this.aircraft.blood = 200;

        this.volume_minus = game.add.text(game.width-110, game.height-43, '-', { font: '18px Arial', fill: '#ffffff' });
        this.volume_minus.inputEnabled = true;
        this.volume_image = game.add.image(game.width-70, game.height-30, 'volume_image'); 
        this.volume_image.anchor.setTo(0.5,0.5);
        this.volume_plus = game.add.text(game.width-40, game.height-40, '+', { font: '18px Arial', fill: '#ffffff' });
        this.volume_plus.inputEnabled = true;
        this.volume_plus.events.onInputDown.add(()=>{
            if(this.bgm.volume<=0.9)this.bgm.volume+=0.1;  
        },this);
        this.volume_minus.events.onInputDown.add(()=>{
            if(this.bgm.volume>=0.1)this.bgm.volume-=0.1;    
        },this);
        /// Particle
        this.small_fireball = game.add.sprite(30,game.height-70,'fireball');
        this.small_fireball.scale.setTo(0.4, 0.4);
        this.small_fireball.anchor.setTo(0.5,0.5);
        this.small_fireball.animations.add('normal',[10],50,false);
        this.small_fireball.animations.play('normal');

        this.pause = game.add.image(game.width-30, 30, 'pause'); 
        this.pause.anchor.setTo(0.5,0.5);
        this.pause.inputEnabled = true;
        this.pause.events.onInputDown.add(()=>{
            game.paused = !game.paused;
        },this);

        this.emitter = game.add.emitter(422, 320, 2);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = -500;

        this.emitter_enemy = game.add.emitter(422, 320, 2);
        this.emitter_enemy.makeParticles('pixel');
        this.emitter_enemy.setYSpeed(-150, 150);
        this.emitter_enemy.setXSpeed(-150, 150);
        this.emitter_enemy.setScale(2, 0, 2, 0, 800);
        this.emitter_enemy.gravity = 500;


        this.graphics_1 = game.add.graphics(0, 0);
        this.graphics_2 = game.add.graphics(0, 0);
        this.graphics_3 = game.add.graphics(0,0);
        this.graphics_4 = game.add.graphics(0,0);
        this.graphics_5 = game.add.graphics(0,0);
        this.graphics_level = game.add.graphics(0,0);
        this.drawpower_edge = game.add.graphics(0,0);
        this.drawpower = game.add.graphics(0,0);
        this.drawaircraft_edge = game.add.graphics(0,0);
        this.power = 0;
        this.power_flag = 0.3;
        this.enter_flag = true;

        this.enter_flag2 = true;

        this.level_flag= 0;

        this.graphics_1.lineStyle(2, 0x555555, 1);
        this.graphics_1.drawRect(60, game.height-42, 200, 24);

        this.drawpower_edge.lineStyle(2, 0x555555, 1);
        this.drawpower_edge.drawRect(60, game.height-42-40, 200, 24);
        if(player_number == 2){
            this.drawaircraft_edge.lineStyle(2, 0x555555, 1);
        this.drawaircraft_edge.drawRect(60, game.height-42-80, 200, 24);
        }
        

        this.level_show = game.add.text(game.width/2, game.height/2, "LEVEL 1", { font: "72px Arial Black", fill: "#000000",align: "center" });
        this.level_show.stroke = "#333333";
        this.level_show.strokeThickness = 8;
        this.level_show.setShadow(2, 2, "#333333", 2, true, false);
        this.level_show.anchor.setTo(0.5);
        this.level_show.alpha = 0;
        
        this.gameover = game.add.text(game.width/2, -100, "Game\nover", { font: "100px Arial Black", fill: "#C00000",align: "center" });
        this.gameover.stroke = "#777733";
        this.gameover.strokeThickness = 8;
        this.gameover.setShadow(2, 2, "#333333", 2, true, false);
        this.gameover.anchor.setTo(0.5);

        this.finish = game.add.text(game.width/2, -100, "Finish", { font: "100px Arial Black", fill: "#DAB273",align: "center" });
        this.finish.stroke = "#E9E9D8";
        this.finish.strokeThickness = 8;
        this.finish.setShadow(2, 2, "#333333", 2, true, false);
        this.finish.anchor.setTo(0.5);

        this.Enemy1 = game.add.group();
        this.black = game.add.sprite(0,0,'black');
        this.black.alpha = 1;
        game.add.tween(this.black).to({alpha:0},1500, Phaser.Easing.Linear.None, true, 1000 ,0);

    
        

        game.physics.arcade.enable(this.airplane);
        game.physics.arcade.enable(this.fireball);
        game.physics.arcade.enable(this.coin);
        game.physics.arcade.enable(this.heart);
        game.physics.arcade.enable(this.helper);
        game.physics.arcade.enable(this.helper_);
        game.physics.arcade.enable(this.aircraft);

    },
    update: function() {
        if (!this.airplane.inWorld) { this.playerDie();}
        this.movePlayer();
        this.moveAircraft();
        this.moveBackground();
        this.drawblood();
        if(player_number == 2)this.drawblood_();
        this.drawpower_();
        if(player_number == 1)this.aircraft.kill();
        if(score>=100)bullet_type = 2;
        if(score>=300){
            this.helper.alpha = 1;
            this.bullet3.fire();
        }
        if(score>=500){
            this.helper_.alpha = 1;
            this.bullet4.fire();
        }
        if(score>=800){
            bullet_type = 3;
        }
        if(bullet_type == 1)BULLET = this.bullet1;
        else if(bullet_type == 2)BULLET = this.bullet2;
        else if(bullet_type)BULLET = this.bullet2;
        game.physics.arcade.overlap(this.airplane, this.coin, this.takeCoin, null, this);
        game.physics.arcade.overlap(this.aircraft, this.coin, this.takeCoin, null, this);
        game.physics.arcade.overlap(this.airplane, this.heart, this.takeHeart, null, this);
        game.physics.arcade.overlap(this.aircraft, this.heart, this.takeHeart_, null, this);

        game.physics.arcade.overlap(BULLET.bullets, this.Enemy1,this.bullet_Enemy1_collide, null, this);
        game.physics.arcade.overlap(BULLET.bullets, this.boss1,this.bullet_Boss1_collide, null, this);
        game.physics.arcade.overlap(this.bullet3.bullets, this.Enemy1,this.helper_bullet_Enemy1_collide, null, this);
        game.physics.arcade.overlap(this.bullet3.bullets, this.boss1,this.helper_bullet_Boss1_collide, null, this);
        game.physics.arcade.overlap(this.bullet4.bullets, this.Enemy1,this.helper__bullet_Enemy1_collide, null, this);
        game.physics.arcade.overlap(this.bullet4.bullets, this.boss1,this.helper___bullet_Boss1_collide, null, this);
        game.physics.arcade.overlap(this.bullet5.bullets, this.Enemy1,this.player2__bullet_Enemy1_collide, null, this);
        game.physics.arcade.overlap(this.bullet5.bullets, this.boss1,this.player2___bullet_Boss1_collide, null, this);
        game.physics.arcade.overlap(this.fireball, this.Enemy1,this.fireball_Enemy1_collide, null, this);
        game.physics.arcade.overlap(this.fireball, this.boss1,this.fireball_Boss1_collide, null, this);
        game.physics.arcade.overlap(this.autoaim_Bullets, this.Enemy1,this.auto_bullet_Enemy1_collide, null, this);
        game.physics.arcade.overlap(this.autoaim_Bullets, this.boss1,this.auto_bullet_Boss1_collide, null, this);
        if(this.fireball.body.y<-50){
            this.fireball.body.x = game.width/2;    
            this.fireball.body.y = game.height; 
            this.fireball.alpha = 0;
            this.enter_flag2 = true;
            console.log("ENTER3");
        }
        if(this.powerButton.isDown && this.power>=200){
            if(this.enter_flag2 == true){
                this.fireball.body.x = this.airplane.body.x-30;
                this.fireball.body.y = this.airplane.body.y-100;
            }
            this.fireball.alpha = 1;
            this.power = 0;          
            this.enter_flag2=false;
            console.log("ENTER1");
        }
        if(this.enter_flag2 == false){
             this.fireball.body.y-=5;
             console.log("ENTER2");
        }
        if(bullet_type != 3){
            if(this.fireButton.isDown && BULLET.fire()){  
            this.emitter.x = this.airplane.x;
            this.emitter.y = this.airplane.y-20;
            this.emitter.start(true,200,null,5);        
               this.shot1.play();
               
        }
        }
        else{
            if(this.fireButton.isDown && game.time.now>autofiringTimer){  
                if(level>=1 && level<=20)this.autoaim_Fires_enemy1();
                else if(level == 1000 || level == 2000 ||level == 3000 ||level == 4000)this.autoaim_Fires_boss1();
                this.emitter.x = this.airplane.x;
                this.emitter.y = this.airplane.y-20;
                this.emitter.start(true,200,null,5);        
                   this.shot1.play();
            }
        }
        
        if(player_number == 2){
            if(this.SHIFT_Button.isDown && this.bullet5.fire()){
            this.emitter.x = this.aircraft.x;
            this.emitter.y = this.aircraft.y-20;
            this.emitter.start(true,200,null,5);        
               this.shot1.play();
           }
        }
        

        if(game.time.now > canbeattackTime) {
            game.physics.arcade.overlap(this.enemy1_Bullets, this.airplane,this.enemy1_Bullet_airplane_collide, null, this);
            game.physics.arcade.overlap(this.boss1_bullet_1.bullets, this.airplane,this.boss1_Bullet1_airplane_collide, null, this);
            game.physics.arcade.overlap(this.boss1_bullet_2.bullets, this.airplane,this.boss1_Bullet2_airplane_collide, null, this);
            game.physics.arcade.overlap(this.boss1_bullet_3.bullets, this.airplane,this.boss1_Bullet3_airplane_collide, null, this);
        }
        if(game.time.now> canbeattackTime_){
            game.physics.arcade.overlap(this.enemy1_Bullets, this.aircraft,this.enemy1_Bullet_aircraft_collide, null, this);
            game.physics.arcade.overlap(this.boss1_bullet_1.bullets, this.aircraft,this.boss1_Bullet1_aircraft_collide, null, this);
            game.physics.arcade.overlap(this.boss1_bullet_2.bullets, this.aircraft,this.boss1_Bullet2_aircraft_collide, null, this);
            game.physics.arcade.overlap(this.boss1_bullet_3.bullets, this.aircraft,this.boss1_Bullet3_aircraft_collide, null, this);
        }
            
        

        if(game.time.now >= 5000 && level==0){
            level = 1;               
            this.levelhandle();
        }
        
        else if(game.time.now >=10000 && level==1 && this.Enemy1.getFirstAlive() == null){
            level = 2;
            this.levelhandle(); 
        }
        else if(level == 2 && this.Enemy1.getFirstAlive() == null){
            level = 3
            this.levelhandle();
        }
        else if(level == 3 && this.Enemy1.getFirstAlive() == null){
            level = 4
            this.levelhandle();  
        }
        else if(level == 4 && this.Enemy1.getFirstAlive() == null){
            level = 5
            this.levelhandle(); 
        }
        else if(level == 5 && this.Enemy1.getFirstAlive() == null){
            level = 1000;
            this.levelhandle();
        }
        
        else if(level == 1000 && this.boss1._exists == false){
            level = 6;
            this.levelhandle();
        }
        else if(level == 6 && this.Enemy1.getFirstAlive() == null){
            level = 7;
            this.levelhandle();  
        }
        else if(level == 7 && this.Enemy1.getFirstAlive() == null){
            level = 8;
            this.levelhandle(); 
        }
        else if(level == 8 && this.Enemy1.getFirstAlive() == null){
            level = 9;
            this.levelhandle();
        }
        else if(level == 9 && this.Enemy1.getFirstAlive() == null){
            level = 10;
            this.levelhandle(); 
        }
        else if(level == 10 && this.Enemy1.getFirstAlive() == null){
            level = 2000;
            this.levelhandle();
        }
        else if(level == 2000 && this.boss1._exists == false){
            level = 11;
            this.levelhandle();
        }
        else if(level == 11 && this.Enemy1.getFirstAlive() == null){
            level = 12;
            this.levelhandle();  
        }
        else if(level == 12 && this.Enemy1.getFirstAlive() == null){
            level = 13;
            this.levelhandle(); 
        }
        else if(level == 13 && this.Enemy1.getFirstAlive() == null){
            level = 14;
            this.levelhandle();
        }
        else if(level == 14 && this.Enemy1.getFirstAlive() == null){
            level = 15;
            this.levelhandle(); 
        }
        else if(level == 15 && this.Enemy1.getFirstAlive() == null){
            level = 3000;
            this.levelhandle();
        }
        else if(level == 3000 && this.boss1._exists == false){
            level = 16;
            this.levelhandle();
        }
        else if(level == 16 && this.Enemy1.getFirstAlive() == null){
            level = 17;
            this.levelhandle();  
        }
        else if(level == 17 && this.Enemy1.getFirstAlive() == null){
            level = 18;
            this.levelhandle(); 
        }
        else if(level == 18 && this.Enemy1.getFirstAlive() == null){
            level = 19;
            this.levelhandle();
        }
        else if(level == 19 && this.Enemy1.getFirstAlive() == null){
            level = 20;
            this.levelhandle(); 
        }
        else if(level == 20 && this.Enemy1.getFirstAlive() == null){
            level = 4000;
            this.levelhandle();
        }
        else if(level == 4000 && this.boss1._exists == false){
            console.log("FInish");
            level = 10000;
            this.playerWin();
        }
        this.graphics_level.clear();
        this.graphics_level.beginFill(0xFF0000, 0.7);
        this.graphics_level.drawRect(0,game.height/2-this.level_flag, game.width, 2*this.level_flag);
        this.graphics_level.endFill(); 
        if(level == 1){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();} 
        }
        else if(level == 2 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}  
        }
        else if(level == 3 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();} 
        }
        else if(level == 4 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}   
        }
        else if(level == 5 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}     
        }
        else if(level == 1000 && create_boss1_finish){
            if(game.time.now > firingTimer){this.boss1_Fires();}
        }
        else if(level == 6 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}  
        }
        else if(level == 7 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();} 
        }
        else if(level == 8 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}   
        }
        else if(level == 9 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}     
        }
        else if(level == 10 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}     
        }
        else if(level == 2000 && create_boss1_finish){
            if(game.time.now > firingTimer){this.boss1_Fires();}
        }
        else if(level == 11 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}  
        }
        else if(level == 12 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();} 
        }
        else if(level == 13 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}   
        }
        else if(level == 14 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}     
        }
        else if(level == 15 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}     
        }
        else if(level == 3000 && create_boss1_finish){
            if(game.time.now > firingTimer){this.boss1_Fires();}
        }
        else if(level == 16 && create_enemy1_finish){
            
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}  
        }
        else if(level == 17 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();} 
        }
        else if(level == 18 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}   
        }
        else if(level == 19 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}     
        }
        else if(level == 20 && create_enemy1_finish){
            if (game.time.now > firingTimer && create_enemy1_finish){this.enemy1_Fires();}     
        }
        else if(level ==4000 && create_boss1_finish){
            if(game.time.now > firingTimer){this.boss1_Fires();}
        }
        
    }, 
    levelhandle: function(){
        console.log("level="+level);
        if(level>=1 && level<=20){
            
            this.createEnemy1();
            this.level_show.text = "Level "+level;
            game.add.tween(this).to({level_flag:40},500, Phaser.Easing.Linear.None, true, 0 ,0).yoyo(true,3000);
            game.add.tween(this.level_show).to({alpha:1},1500, Phaser.Easing.Linear.None, true, 0 ,0).yoyo(true,1000);
        }else{
            this.createBoss1();
            if(level!=4000)this.level_show.text = "Boss";
            else this.level_show.text = "Final Boss";
            game.add.tween(this).to({level_flag:40},500, Phaser.Easing.Linear.None, true, 0 ,0).yoyo(true,3000);
            game.add.tween(this.level_show).to({alpha:1},1500, Phaser.Easing.Linear.None, true, 0 ,0).yoyo(true,1000);  
        }
    },
    drawpower_: function(){
        this.drawpower.clear();
        
        if(this.power >= 200 && this.enter_flag){
            game.add.tween(this).to({power_flag:1},200, Phaser.Easing.Linear.None, true,0,-1).yoyo(true,200);
            this.enter_flag = false;
        }
        this.drawpower.beginFill(0x0000FF, this.power_flag);
        this.drawpower.drawRect(60, game.height-42-40, this.power, 24);
        this.drawpower.endFill();
        if(this.power<200)this.power+=0.4;
        
    },
    drawblood: function(){
        //console.log(graphics);
        
        //this.graphics_2.lineStyle(2, 0x555555, 1);
        this.graphics_2.clear();
        this.graphics_2.beginFill(0xFF0000, 0.5);
        this.graphics_2.drawRect(60, game.height-42, this.airplane.blood, 24);
        this.graphics_2.endFill();

        //console.log("blood="+this.airplane.blood+" x="+x);
        if(this.airplane.blood<=x){
            x-=0.2;
        }
        this.graphics_3.clear();
        this.graphics_3.beginFill(0xFF0000, 0.5);
        this.graphics_3.drawRect(60, game.height-42, x, 24);
        //x-=1;
        this.graphics_3.endFill();

     
    },
    drawblood_: function(){
        //console.log(graphics);
        
        //this.graphics_2.lineStyle(2, 0x555555, 1);
        this.graphics_4.clear();
        this.graphics_4.beginFill(0xFF0000, 0.5);
        this.graphics_4.drawRect(60, game.height-42-80, this.aircraft.blood, 24);
        this.graphics_4.endFill();

        //console.log("blood="+this.airplane.blood+" x="+x);
        if(this.aircraft.blood<=x_){
            x_-=0.2;
        }
        this.graphics_5.clear();
        this.graphics_5.beginFill(0xFF0000, 0.5);
        this.graphics_5.drawRect(60, game.height-42-80, x_, 24);
        //x-=1;
        this.graphics_5.endFill();

     
    },
    playerDie: function() { 

        this.bgm.stop();
        firebase.database().ref('rank/'+username).set({score:score});      
        this.airplane.kill();
        game.add.tween(this.gameover).to({y: game.height/2},1500, Phaser.Easing.Bounce.Out, true, 1000 ,0).onComplete.add(()=>{
            game.add.tween(this.black).to({alpha:1},1500, Phaser.Easing.Linear.None, true, 500 ,0).onComplete.add(()=>{
                game.state.start('end');
            },this);
              
          },this);
        
    },
    playerWin: function() { 

        this.bgm.stop();
           
        game.add.tween(this.finish).to({y: game.height/2},1500, Phaser.Easing.Bounce.Out, true, 1000 ,0).onComplete.add(()=>{
            game.add.tween(this.black).to({alpha:1},1500, Phaser.Easing.Linear.None, true, 500 ,0).onComplete.add(()=>{
                firebase.database().ref('rank/'+username).set({score:score});   
                game.state.start('end');
            },this);
              
          },this);
        
    },
    moveBackground: function() {
        if(this.cursor.left.isDown && this.back.centerX>-110){
            this.back.centerX -= 0.5;
        }
        if(this.cursor.right.isDown && this.back.centerX<760){
            this.back.centerX += 0.5;
        }
        if(this.cursor.up.isDown && this.back.centerY>270){
            this.back.centerY -= 0.2;
        }
        if(this.cursor.down.isDown && this.back.centerY<430){
            this.back.centerY += 0.2;
        }

    },
    movePlayer: function() {
        if (this.cursor.left.isDown && this.airplane.body.x>0) {
            this.airplane.body.velocity.x = -200;
            this.airplane.animations.play('left');
            this.helper.body.velocity.x = -200;
            this.helper_.body.velocity.x = -200;
            ///
        }
        else if (this.cursor.right.isDown && this.airplane.body.x<game.width-70) { 
            this.airplane.body.velocity.x = 200;
            this.airplane.animations.play('right');
            this.helper.body.velocity.x = 200;
            this.helper_.body.velocity.x = 200;
            ///
        }   
        else{
            this.airplane.body.velocity.x = 0;
            this.airplane.animations.play('normal');
            this.helper.body.velocity.x = 0;
            this.helper_.body.velocity.x = 0;
        } 

        // If the up arrow key is pressed, And the player is on the ground.
        if (this.cursor.up.isDown && this.airplane.body.y>0) { 
            this.airplane.body.velocity.y = -200;
            this.airplane.animations.play('forward');
            this.helper.body.velocity.y = -200;
            this.helper_.body.velocity.y = -200;
        }  
        else if (this.cursor.down.isDown && this.airplane.body.y<game.height-65) { 
            this.airplane.body.velocity.y = 200;
            this.airplane.animations.play('backward');
            this.helper.body.velocity.y = 200;
            this.helper_.body.velocity.y = 200;

        }  
        // If neither the right or left arrow key is pressed
        else {
            this.airplane.body.velocity.y = 0;
            this.helper.body.velocity.y = 0;
            this.helper_.body.velocity.y = 0;
        } 
    },
    moveAircraft: function() {
        if (this.A_Button.isDown && this.aircraft.body.x>0) {
            
            this.aircraft.body.velocity.x = -200;
            ///
        }
        else if (this.D_Button.isDown && this.aircraft.body.x<game.width-70) { 
            this.aircraft.body.velocity.x = 200;
            ///
        }   
        else{
            this.aircraft.body.velocity.x = 0;
        } 

        // If the up arrow key is pressed, And the player is on the ground.
        if (this.W_Button.isDown && this.aircraft.body.y>0) { 
            this.aircraft.body.velocity.y = -200;
        }  
        else if (this.S_Button.isDown && this.aircraft.body.y<game.height-65) { 
            this.aircraft.body.velocity.y = 200;
        }  
        // If neither the right or left arrow key is pressed
        else {
            this.aircraft.body.velocity.y = 0;
        } 
    },
    takeCoin: function(){
        game.time.events.add(Phaser.Timer.SECOND * 2, this.generateCoin, this);
        this.coin.kill();     
        score += 10;    
        this.scoreLabel.text = 'score: ' + score; 
    },
    generateCoin: function(){
        this.coin.reset(game.rnd.integerInRange(30,620),game.rnd.integerInRange(200,550));
        this.coin.alpha = 0;
        game.add.tween(this.coin).to( { alpha: 1 }, 2000, "Linear", true);
    },
    takeHeart: function(){
        game.time.events.add(Phaser.Timer.SECOND * 10, this.generateHeart, this);
        this.heart.kill();     
        if(this.airplane.blood>=170){this.airplane.blood = 200;x = 200;}
        else {this.airplane.blood += 30;x+=30;}     
    },
    takeHeart_: function(){
        game.time.events.add(Phaser.Timer.SECOND * 10, this.generateHeart, this);
        this.heart.kill();     
        if(this.aircraft.blood>=170){this.aircraft.blood = 200;x_ = 200;}
        else {this.aircraft.blood += 30;x_+=30;}     
    },
    generateHeart: function(){
        this.heart.reset(game.rnd.integerInRange(30,620),game.rnd.integerInRange(200,550));
        this.heart.alpha = 0;
        game.add.tween(this.heart).to( { alpha: 1 }, 2000, "Linear", true);
    },
    createEnemy1: function(){
        this.Enemy1 = game.add.group();
        this.Enemy1.enableBody = true;
        this.Enemy1.physicsBodyType = Phaser.Physics.ARCADE;
        var imgsrc = 'enemy1';
        if(level ==1 || level == 6 || level == 11|| level == 16)imgsrc = 'enemy1_1';
        else if(level == 2|| level == 7 || level == 12|| level == 17)imgsrc = 'enemy1_2';
        else if(level == 3|| level == 8 || level == 13|| level == 18)imgsrc = 'enemy1_3';
        else if(level == 4|| level == 9 || level == 14|| level == 19)imgsrc = 'enemy1_4';
        else if(level == 5|| level == 10 || level == 15|| level == 20)imgsrc = 'enemy1_5';
        for(var x=0;x<4;x++){
            var enemy1 = this.Enemy1.create(x * 100, 0 , imgsrc);
            enemy1.anchor.setTo(0.5, 0.5);
            enemy1.scale.setTo(1.2,1.2);
            enemy1.body.moves = false;
            enemy1.blood = 100+level*25;
        }
        this.Enemy1.x=game.width/2-155-80;
        this.Enemy1.y=-20;
        this.Enemy1_tween1 = game.add.tween(this.Enemy1).to( { y: 80 }, 2000, Phaser.Easing.Linear.None, true);
        this.Enemy1_tween1.onComplete.addOnce(this.Enemy1_tween, this);
    },
    Enemy1_tween: function(){
        create_enemy1_finish = true;
        if(level>=1 && level<=5)this.Enemy1_tween2 = game.add.tween(this.Enemy1).to( { x: game.width/2-155+120 }, 2000-level*50, Phaser.Easing.Linear.None, true, 0, 1000, true).onComplete.add(()=>{},this);
        if(level>=6 && level<=20)this.Enemy1_tween2 = game.add.tween(this.Enemy1).to( { x: game.width/2-155+120 }, 2000-level*50, Phaser.Easing.Cubic.Out, true, 0, 1000, true).onComplete.add(()=>{},this);
       
    },
    createBoss1:function(){
        var quake = game.add.tween(game.camera).to({x: game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 7,true);
        quake.start();
        this.boss1.blood = 1000+level*2.5;
        this.boss1.reset(game.width/2,-200);
        if(level == 1000)this.boss1.loadTexture("boss1","boss1",false);
        else if(level == 2000)this.boss1.loadTexture("boss2","boss2",false);
        else if( level == 3000 )this.boss1.loadTexture("boss3","boss3",false);
        else if( level == 4000 )this.boss1.loadTexture("boss4","boss4",false);
        
        if(level == 1000 || level == 3000|| level == 4000)this.boss1_tween1 = game.add.tween(this.boss1).to( { y: 120 }, 2000, Phaser.Easing.Linear.None, true).onComplete.add(this.Boss1_tween,this);
        else if(level == 2000)this.boss1_tween1 = game.add.tween(this.boss1).to( { y: 150 }, 2000, Phaser.Easing.Linear.None, true).onComplete.add(this.Boss1_tween,this);
    },
    Boss1_tween: function(){
        create_boss1_finish = true;
        if(level==3000)this.boss1_tween2 = game.add.tween(this.boss1).to( { x: 250 }, 2000, Phaser.Easing.Cubic.Out, true).onComplete.add(()=>{
            this.boss1_tween3 = game.add.tween(this.boss1).to( { x: game.width-250 }, 2000, Phaser.Easing.Cubic.Out, true, 0, 1000, true)
        },this);
        if(level==4000)this.boss1_tween2 = game.add.tween(this.boss1).to( { x: 250 }, 1600, Phaser.Easing.Cubic.Out, true).onComplete.add(()=>{
            this.boss1_tween3 = game.add.tween(this.boss1).to( { x: game.width-250 }, 1600, Phaser.Easing.Cubic.Out, true, 0, 1000, true)
        },this);
        
    },
    boss1_Fires:function(){
        if(boss1_mode == 0 && game.time.now>modeTimer){
                modeTimer = game.time.now+20000;
                boss1_mode = 1;
                //console.log("mode="+boss1_mode);
                this.boss1.tint = "0xffffff";
        }
        else if(boss1_mode == 1 && game.time.now>modeTimer){
            modeTimer = game.time.now+1000;
            this.angle_random1 = game.rnd.integerInRange(0,60);
            this.angle_random2 = game.rnd.integerInRange(0,60);
            this.angle_random3 = game.rnd.integerInRange(0,60);
            //console.log(this.angle_random1+" "+this.angle_random2);
            boss1_mode = 2;
            //console.log("mode="+boss1_mode);
            game.add.tween(this.boss1.scale).to({x:0.6,y:0.6},500, Phaser.Easing.Linear.None, true).yoyo(true,7000);
            this.boss1.tint = "0xff0000";
            //game.add.tween(this.Enemy1.getChildAt(i)).to( {tint:0xCC0000}, 10, Phaser.Easing.Linear.None, true)
        }
        else if(boss1_mode == 2 && game.time.now>modeTimer){
            modeTimer = game.time.now+6000;
            boss1_mode = 3;
            //console.log("mode="+boss1_mode);
        }
        else if(boss1_mode == 3 && game.time.now>modeTimer){
            modeTimer = game.time.now+20000;
            boss1_mode = 1;
            //console.log("mode="+boss1_mode);
            this.boss1.tint = "0xffffff";
        }
        if(boss1_mode == 1 || boss1_mode == 2){
            this.boss1_bullet_1.trackSprite(this.boss1, 0, 85);
        this.boss1_bullet_1.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.boss1_bullet_1.fireAngle = 75;
        this.boss1_bullet_1.bulletAngleVariance = 30;
        this.boss1_bullet_1.bulletSpeed = 400;
        this.boss1_bullet_1.fireRate = 400;
        this.boss1_bullet_1.fire();

        this.boss1_bullet_2.trackSprite(this.boss1, 0, 85);
        this.boss1_bullet_2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.boss1_bullet_2.fireAngle = 105;
        this.boss1_bullet_2.bulletAngleVariance = 30;
        this.boss1_bullet_2.bulletSpeed = 400;
        this.boss1_bullet_2.fireRate = 400;
        this.boss1_bullet_2.fire();
        if(level == 4000){
        this.boss1_bullet_3.trackSprite(this.boss1, 0, 85);
        this.boss1_bullet_3.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.boss1_bullet_3.fireAngle = 90;
        this.boss1_bullet_3.bulletAngleVariance = 30;
        this.boss1_bullet_3.bulletSpeed = 400;
        this.boss1_bullet_3.fireRate = 400;
        this.boss1_bullet_3.fire();
        }
        this.emitter_enemy.x = this.boss1.x;
        this.emitter_enemy.y = this.boss1.y+85;
        this.emitter_enemy.start(true,200,null,5);

        firingTimer = game.time.now + 1000;
        }
        else if(boss1_mode == 3){
            this.boss1_bullet_1.trackSprite(this.boss1, 0, 100);
        this.boss1_bullet_1.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.boss1_bullet_1.fireAngle = 60+this.angle_random1;
        this.boss1_bullet_1.bulletAngleVariance = 0;
        this.boss1_bullet_1.bulletSpeed = 400;
        this.boss1_bullet_1.fireRate = 150;
        this.boss1_bullet_1.fire();

        this.boss1_bullet_2.trackSprite(this.boss1, 0, 100);
        this.boss1_bullet_2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.boss1_bullet_2.fireAngle = 60+this.angle_random2;
        this.boss1_bullet_2.bulletAngleVariance = 0;
        this.boss1_bullet_2.bulletSpeed = 400;
        this.boss1_bullet_2.fireRate = 150;
        this.boss1_bullet_2.fire();
        if(level == 4000){
        this.boss1_bullet_3.trackSprite(this.boss1, 0, 100);
        this.boss1_bullet_3.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.boss1_bullet_3.fireAngle = 60+this.angle_random3;
        this.boss1_bullet_3.bulletAngleVariance = 0;
        this.boss1_bullet_3.bulletSpeed = 400;
        this.boss1_bullet_3.fireRate = 150;
        this.boss1_bullet_3.fire();
        }
        this.emitter_enemy.x = this.boss1.x;
        this.emitter_enemy.y = this.boss1.y+100;
        this.emitter_enemy.start(true,200,null,5);

        firingTimer = game.time.now + 200;
        }
        
    },
    autoaim_Fires_enemy1:function() {

        autoaimBullet = this.autoaim_Bullets.getFirstExists(false);    
        var distance = 1000;
        livingEnemies.length=0;  
        if(this.Enemy1!=null){
            var airplane_x = this.airplane.body.x;
            var offset = this.Enemy1.x;
            this.Enemy1.forEachAlive(function(enemy1){
                livingEnemies.push(enemy1);
                //console.log(game.math.difference(enemy1.body.x,airplane_x));
                if(game.math.difference(enemy1.body.x,airplane_x)<distance){
                    distance = game.math.difference(enemy1.body.x,airplane_x)
                    targetEnemy = enemy1;
                }                
            });  
             var another = targetEnemy;
             another.x+=offset;
            if (autoaimBullet && livingEnemies.length > 0)
            {              
                autoaimBullet.reset(this.airplane.body.x+37,this.airplane.body.y+20);  
                game.physics.arcade.moveToObject(autoaimBullet,another,800);
                another.x-=offset;
                this.emitter_enemy.x = autoaimBullet.x;
                this.emitter_enemy.y = autoaimBullet.y;
                this.emitter_enemy.start(true,200,null,5);
                autofiringTimer = game.time.now + 50;
            }
        }
        else{            
                autoaimBullet.reset(this.airplane.body.x+37,this.airplane.body.y+20);
                autoaimBullet.body.velocity.y = -800;
                //console.log(autoaimBullet);
                this.emitter_enemy.x = autoaimBullet.x;
                this.emitter_enemy.y = autoaimBullet.y;
                this.emitter_enemy.start(true,200,null,5);
                autofiringTimer = game.time.now + 50;          
        }   
    },
    autoaim_Fires_boss1:function() {

        autoaimBullet = this.autoaim_Bullets.getFirstExists(false);    
        if(create_boss1_finish){
            if (autoaimBullet)
            {              
                autoaimBullet.reset(this.airplane.body.x+37,this.airplane.body.y+20);  
                game.physics.arcade.moveToObject(autoaimBullet,this.boss1,800);
                this.emitter_enemy.x = autoaimBullet.x;
                this.emitter_enemy.y = autoaimBullet.y;
                this.emitter_enemy.start(true,200,null,5);
                autofiringTimer = game.time.now + 50;
            }
        }
        else{            
                autoaimBullet.reset(this.airplane.body.x+37,this.airplane.body.y+20);
                autoaimBullet.body.velocity.y = -800;
                //console.log(autoaimBullet);
                this.emitter_enemy.x = autoaimBullet.x;
                this.emitter_enemy.y = autoaimBullet.y;
                this.emitter_enemy.start(true,200,null,5);
                autofiringTimer = game.time.now + 50;          
        }   
    },
    enemy1_Fires:function() {

        //  Grab the first bullet we can from the pool
        var select_airplane = game.rnd.integerInRange(0,1);
        enemyBullet = this.enemy1_Bullets.getFirstExists(false);    
        livingEnemies.length=0;   
        this.Enemy1.forEachAlive(function(enemy1){
            // put every living enemy in an array
            livingEnemies.push(enemy1);
        });  
        if (enemyBullet && livingEnemies.length > 0)
        {              
            // randomly select one of them
            var shooter=livingEnemies[game.rnd.integerInRange(0,livingEnemies.length-1)];
            //game.physics.arcade.enable(shooter);
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x+32, shooter.body.y+50);
            if(player_number == 1)game.physics.arcade.moveToObject(enemyBullet,this.airplane,105+level*20);
            else{
                if(this.airplane._exists && this.aircraft._exists){
                    if(select_airplane == 0)game.physics.arcade.moveToObject(enemyBullet,this.airplane,105+level*20);
                    else if(select_airplane == 1)game.physics.arcade.moveToObject(enemyBullet,this.aircraft,105+level*20);
                }
                else if(this.airplane._exists)game.physics.arcade.moveToObject(enemyBullet,this.airplane,105+level*20);
                else if(this.aircraft._exists)game.physics.arcade.moveToObject(enemyBullet,this.aircraft,105+level*20);
            }
            this.emitter_enemy.x = enemyBullet.x;
            this.emitter_enemy.y = enemyBullet.y;
            this.emitter_enemy.start(true,200,null,5);
            firingTimer = game.time.now + 1800-level*60;
        }
    
    },
    fireball_Enemy1_collide:function(){
        var flag = 0;
        for(var i=0;i<this.Enemy1.hash.length;i++){
                 game.physics.arcade.overlap(this.fireball,this.Enemy1.getChildAt(i),()=>{

                    console.log("#####");
                    this.Enemy1.getChildAt(i).blood-=100;
                    if(this.Enemy1.getChildAt(i).blood<=0){
                        this.Enemy1.getChildAt(i).kill();
                        score += 5;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions.getFirstExists(false);
                    explosion.reset(this.Enemy1.getChildAt(i).body.x+30, this.Enemy1.getChildAt(i).body.y+30);
                    explosion.play('boom', 30, false, true);
                    this.fireball.alpha = 0; 
                    this.enter_flag2 = true;  
                    this.fireball.body.x = game.width/2;    
                    this.fireball.body.y = game.height;           
                    game.add.tween(this.Enemy1.getChildAt(i)).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
          }
    },
    fireball_Boss1_collide:function(){

                    this.boss1.blood-=100;
                    if(this.boss1.blood<=0){
                        this.boss1.kill();
                        score += 30;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions_big.getFirstExists(false);
                    explosion.reset(this.boss1.body.x+120, this.boss1.body.y+120);
                    explosion.play('boom_big', 30, false, true);
                    this.fireball.alpha = 0; 
                    this.enter_flag2 = true;  
                    this.fireball.body.x = game.width/2;    
                    this.fireball.body.y = game.height;            
                    game.add.tween(this.boss1).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);

    },
    bullet_Enemy1_collide:function(){
        var flag = 0;
            for(var i=0;i<this.Enemy1.hash.length;i++){
            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(BULLET.bullets.getChildAt(j),this.Enemy1.getChildAt(i),()=>{
                    this.Enemy1.getChildAt(i).blood-=10;
                    if(this.Enemy1.getChildAt(i).blood<=0){
                        this.Enemy1.getChildAt(i).kill();
                        score += 5;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions.getFirstExists(false);
                    
                    explosion.reset(this.Enemy1.getChildAt(i).body.x+30, this.Enemy1.getChildAt(i).body.y+30);
                    explosion.play('boom', 30, false, true);
                    BULLET.bullets.getChildAt(j).kill();                   
                    game.add.tween(this.Enemy1.getChildAt(i)).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }if(flag==1)break;
          }
        flag=0;    
    },
    bullet_Boss1_collide:function(){
        var flag = 0;

            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(BULLET.bullets.getChildAt(j),this.boss1,()=>{
                    this.boss1.blood-=10;
                    if(this.boss1.blood<=0){
                        this.boss1.kill();
                        
                        score += 30;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions_big.getFirstExists(false);
                    explosion.reset(this.boss1.body.x+120, this.boss1.body.y+120);
                    explosion.play('boom_big', 30, false, true)
                    BULLET.bullets.getChildAt(j).kill(); //isFinished isPlaying
                    game.add.tween(this.boss1).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }
        flag=0;    
    },
    helper_bullet_Enemy1_collide:function(){
        var flag = 0;
            for(var i=0;i<this.Enemy1.hash.length;i++){
            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(this.bullet3.bullets.getChildAt(j),this.Enemy1.getChildAt(i),()=>{
                    this.Enemy1.getChildAt(i).blood-=10;
                    if(this.Enemy1.getChildAt(i).blood<=0){
                        this.Enemy1.getChildAt(i).kill();
                        score += 5;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions.getFirstExists(false);
                    
                    explosion.reset(this.Enemy1.getChildAt(i).body.x+30, this.Enemy1.getChildAt(i).body.y+30);
                    explosion.play('boom', 30, false, true);
                    this.bullet3.bullets.getChildAt(j).kill();                   
                    game.add.tween(this.Enemy1.getChildAt(i)).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }if(flag==1)break;
          }
        flag=0;    
    },
    helper_bullet_Boss1_collide:function(){
        var flag = 0;

            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(this.bullet3.bullets.getChildAt(j),this.boss1,()=>{
                    this.boss1.blood-=10;
                    if(this.boss1.blood<=0){
                        this.boss1.kill();
                        
                        score += 30;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions_big.getFirstExists(false);
                    explosion.reset(this.boss1.body.x+120, this.boss1.body.y+120);
                    explosion.play('boom_big', 30, false, true)
                    this.bullet3.bullets.getChildAt(j).kill(); //isFinished isPlaying
                    game.add.tween(this.boss1).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }
        flag=0;    
    },
    helper__bullet_Enemy1_collide:function(){
        var flag = 0;
            for(var i=0;i<this.Enemy1.hash.length;i++){
            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(this.bullet4.bullets.getChildAt(j),this.Enemy1.getChildAt(i),()=>{
                    this.Enemy1.getChildAt(i).blood-=10;
                    if(this.Enemy1.getChildAt(i).blood<=0){
                        this.Enemy1.getChildAt(i).kill();
                        score += 5;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions.getFirstExists(false);
                    
                    explosion.reset(this.Enemy1.getChildAt(i).body.x+30, this.Enemy1.getChildAt(i).body.y+30);
                    explosion.play('boom', 30, false, true);
                    this.bullet4.bullets.getChildAt(j).kill();                   
                    game.add.tween(this.Enemy1.getChildAt(i)).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }if(flag==1)break;
          }
        flag=0;    
    },
    auto_bullet_Enemy1_collide:function(){
        var flag = 0;
            for(var i=0;i<this.Enemy1.hash.length;i++){
            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(this.autoaim_Bullets.getChildAt(j),this.Enemy1.getChildAt(i),()=>{
                    this.Enemy1.getChildAt(i).blood-=10;
                    if(this.Enemy1.getChildAt(i).blood<=0){
                        this.Enemy1.getChildAt(i).kill();
                        score += 5;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions.getFirstExists(false);
                    
                    explosion.reset(this.Enemy1.getChildAt(i).body.x+30, this.Enemy1.getChildAt(i).body.y+30);
                    explosion.play('boom', 30, false, true);
                    this.autoaim_Bullets.getChildAt(j).kill();                   
                    game.add.tween(this.Enemy1.getChildAt(i)).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }if(flag==1)break;
          }
        flag=0;    
    },
    player2__bullet_Enemy1_collide:function(){
        var flag = 0;
            for(var i=0;i<this.Enemy1.hash.length;i++){
            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(this.bullet5.bullets.getChildAt(j),this.Enemy1.getChildAt(i),()=>{
                    this.Enemy1.getChildAt(i).blood-=10;
                    if(this.Enemy1.getChildAt(i).blood<=0){
                        this.Enemy1.getChildAt(i).kill();
                        score += 5;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions.getFirstExists(false);
                    
                    explosion.reset(this.Enemy1.getChildAt(i).body.x+30, this.Enemy1.getChildAt(i).body.y+30);
                    explosion.play('boom', 30, false, true);
                    this.bullet5.bullets.getChildAt(j).kill();                   
                    game.add.tween(this.Enemy1.getChildAt(i)).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }if(flag==1)break;
          }
        flag=0;    
    },
    helper__bullet_Boss1_collide:function(){
        var flag = 0;

            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(this.bullet4.bullets.getChildAt(j),this.boss1,()=>{
                    this.boss1.blood-=10;
                    if(this.boss1.blood<=0){
                        this.boss1.kill();
                        
                        score += 30;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions_big.getFirstExists(false);
                    explosion.reset(this.boss1.body.x+120, this.boss1.body.y+120);
                    explosion.play('boom_big', 30, false, true)
                    this.bullet4.bullets.getChildAt(j).kill(); //isFinished isPlaying
                    game.add.tween(this.boss1).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }
        flag=0;    
    },
    auto_bullet_Boss1_collide:function(){
        var flag = 0;

            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(this.autoaim_Bullets.getChildAt(j),this.boss1,()=>{
                    this.boss1.blood-=10;
                    if(this.boss1.blood<=0){
                        this.boss1.kill();
                        
                        score += 30;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions_big.getFirstExists(false);
                    explosion.reset(this.boss1.body.x+120, this.boss1.body.y+120);
                    explosion.play('boom_big', 30, false, true)
                    this.autoaim_Bullets.getChildAt(j).kill(); //isFinished isPlaying
                    game.add.tween(this.boss1).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }
        flag=0;    
    },
    player2__bullet_Boss1_collide:function(){
        var flag = 0;

            for(var j=0;j<30;j++){
                 game.physics.arcade.overlap(this.bullet5.bullets.getChildAt(j),this.boss1,()=>{
                    this.boss1.blood-=10;
                    if(this.boss1.blood<=0){
                        this.boss1.kill();
                        
                        score += 30;    
                        this.scoreLabel.text = 'score: ' + score; 
                    }
                    var explosion = this.explosions_big.getFirstExists(false);
                    explosion.reset(this.boss1.body.x+120, this.boss1.body.y+120);
                    explosion.play('boom_big', 30, false, true)
                    this.bullet5.bullets.getChildAt(j).kill(); //isFinished isPlaying
                    game.add.tween(this.boss1).to( {tint:0xCC0000}, 2, Phaser.Easing.Linear.None, true).yoyo(true,2);
                    flag = 1;
                }, null, this);
                if(flag==1)break;
             }
        flag=0;    
    },
    enemy1_Bullet_airplane_collide:function(){
        for(var j=0;j<30;j++){
            game.physics.arcade.overlap(this.enemy1_Bullets.getChildAt(j),this.airplane,()=>{
                if(this.airplane.blood>=10)this.airplane.blood-=10;
                else this.airplane.blood=0;             
                var quake = game.add.tween(game.camera).to({x: game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 3,true);
                quake.start();
                this.enemy1_Bullets.getChildAt(j).kill();
                var explosion = this.explosions.getFirstExists(false);
                explosion.reset(this.airplane.body.x+35, this.airplane.body.y+30);
                explosion.play('boom', 30, false, true);
                if(this.airplane.blood<=0){
                    this.airplane.kill();
                    if(player_number == 1)this.playerDie();
                    else if(player_number == 2 && !this.aircraft._exists)this.playerDie();
                }
                game.add.tween(this.airplane).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                    game.add.tween(this.airplane).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                        game.add.tween(this.airplane).to( {alpha:0,tint:0x220000 }, 100, Phaser.Easing.Linear.None, true).yoyo(true,100)
                    });
                });
                canbeattackTime = game.time.now+1500;
            },null,this);
        }
    },
    enemy1_Bullet_aircraft_collide:function(){
        for(var j=0;j<30;j++){
            game.physics.arcade.overlap(this.enemy1_Bullets.getChildAt(j),this.aircraft,()=>{
                if(this.aircraft.blood>=10)this.aircraft.blood-=10;
                else this.aircraft.blood=0;             
                var quake = game.add.tween(game.camera).to({x: game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 3,true);
                quake.start();
                this.enemy1_Bullets.getChildAt(j).kill();
                var explosion = this.explosions.getFirstExists(false);
                explosion.reset(this.aircraft.body.x+35, this.aircraft.body.y+30);
                explosion.play('boom', 30, false, true);
                if(this.aircraft.blood<=0){
                    this.aircraft.kill();
                    if(!this.airplane._exists)this.playerDie();
                }
                game.add.tween(this.aircraft).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                    game.add.tween(this.aircraft).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                        game.add.tween(this.aircraft).to( {alpha:0,tint:0x220000 }, 100, Phaser.Easing.Linear.None, true).yoyo(true,100)
                    });
                });
                canbeattackTime_ = game.time.now+1500;
            },null,this);
        }
    },
    boss1_Bullet1_airplane_collide:function(){
        for(var j=0;j<30;j++){
            game.physics.arcade.overlap(this.boss1_bullet_1.bullets.getChildAt(j),this.airplane,()=>{
                if(this.airplane.blood>=20)this.airplane.blood-=20;
                else this.airplane.blood=0;
                var quake = game.add.tween(game.camera).to({x: game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 3,true);
                quake.start();           
                this.boss1_bullet_1.bullets.getChildAt(j).kill();
                var explosion = this.explosions.getFirstExists(false);
                explosion.reset(this.airplane.body.x+35, this.airplane.body.y+30);
                explosion.play('boom', 30, false, true);
                if(this.airplane.blood<=0){
                    this.airplane.kill();
                    if(player_number == 1)this.playerDie();
                    else if(player_number == 2 && !this.aircraft._exists)this.playerDie();
                }
                game.add.tween(this.airplane).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                    game.add.tween(this.airplane).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                        game.add.tween(this.airplane).to( {alpha:0,tint:0x220000 }, 100, Phaser.Easing.Linear.None, true).yoyo(true,100)
                    });
                });
                canbeattackTime = game.time.now+1500;
                
            },null,this);
        }
    },
    boss1_Bullet1_aircraft_collide:function(){
        for(var j=0;j<30;j++){
            game.physics.arcade.overlap(this.boss1_bullet_1.bullets.getChildAt(j),this.aircraft,()=>{
                if(this.aircraft.blood>=20)this.aircraft.blood-=20;
                else this.aircraft.blood=0;
                var quake = game.add.tween(game.camera).to({x: game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 3,true);
                quake.start();           
                this.boss1_bullet_1.bullets.getChildAt(j).kill();
                var explosion = this.explosions.getFirstExists(false);
                explosion.reset(this.aircraft.body.x+35, this.aircraft.body.y+30);
                explosion.play('boom', 30, false, true);
                if(this.aircraft.blood<=0){
                    this.aircraft.kill();
                    if(!this.airplane._exists)this.playerDie();
                }
                game.add.tween(this.aircraft).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                    game.add.tween(this.aircraft).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                        game.add.tween(this.aircraft).to( {alpha:0,tint:0x220000 }, 100, Phaser.Easing.Linear.None, true).yoyo(true,100)
                    });
                });
                canbeattackTime_ = game.time.now+1500;
                
            },null,this);
        }
    },
    boss1_Bullet2_airplane_collide:function(){
        for(var j=0;j<30;j++){
            game.physics.arcade.overlap(this.boss1_bullet_2.bullets.getChildAt(j),this.airplane,()=>{
                if(this.airplane.blood>=20)this.airplane.blood-=20;
                else this.airplane.blood=0;
                var quake = game.add.tween(game.camera).to({x: game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 3,true);
                quake.start();             
                this.boss1_bullet_2.bullets.getChildAt(j).kill();
                var explosion = this.explosions.getFirstExists(false);
                explosion.reset(this.airplane.body.x+35, this.airplane.body.y+30);
                explosion.play('boom', 30, false, true);
                if(this.airplane.blood<=0){
                    this.airplane.kill();
                    this.playerDie();
                }
                game.add.tween(this.airplane).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                    game.add.tween(this.airplane).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                        game.add.tween(this.airplane).to( {alpha:0,tint:0x220000 }, 100, Phaser.Easing.Linear.None, true).yoyo(true,100)
                    });
                });
                canbeattackTime = game.time.now+1500;
                
            },null,this);
        }
    },
    boss1_Bullet2_aircraft_collide:function(){
        for(var j=0;j<30;j++){
            game.physics.arcade.overlap(this.boss1_bullet_2.bullets.getChildAt(j),this.aircraft,()=>{
                if(this.aircraft.blood>=20)this.aircraft.blood-=20;
                else this.aircraft.blood=0;
                var quake = game.add.tween(game.camera).to({x: game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 3,true);
                quake.start();             
                this.boss1_bullet_2.bullets.getChildAt(j).kill();
                var explosion = this.explosions.getFirstExists(false);
                explosion.reset(this.aircraft.body.x+35, this.aircraft.body.y+30);
                explosion.play('boom', 30, false, true);
                if(this.aircraft.blood<=0){
                    this.aircraft.kill();
                    if(!this.airplane._exists)this.playerDie();
                }
                game.add.tween(this.aircraft).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                    game.add.tween(this.aircraft).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                        game.add.tween(this.aircraft).to( {alpha:0,tint:0x220000 }, 100, Phaser.Easing.Linear.None, true).yoyo(true,100)
                    });
                });
                canbeattackTime_ = game.time.now+1500;
                
            },null,this);
        }
    },
    boss1_Bullet3_airplane_collide:function(){
        for(var j=0;j<30;j++){
            game.physics.arcade.overlap(this.boss1_bullet_3.bullets.getChildAt(j),this.airplane,()=>{
                if(this.airplane.blood>=20)this.airplane.blood-=20;
                else this.airplane.blood=0;
                var quake = game.add.tween(game.camera).to({x: game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 3,true);
                quake.start();             
                this.boss1_bullet_3.bullets.getChildAt(j).kill();
                var explosion = this.explosions.getFirstExists(false);
                explosion.reset(this.airplane.body.x+35, this.airplane.body.y+30);
                explosion.play('boom', 30, false, true);
                if(this.airplane.blood<=0){
                    this.airplane.kill();
                    if(player_number == 1)this.playerDie();
                    else if(player_number == 2 && !this.aircraft._exists)this.playerDie();
                }
                game.add.tween(this.airplane).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                    game.add.tween(this.airplane).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                        game.add.tween(this.airplane).to( {alpha:0,tint:0x220000 }, 100, Phaser.Easing.Linear.None, true).yoyo(true,100)
                    });
                });
                canbeattackTime = game.time.now+1500;
                
            },null,this);
        }
    },
    boss1_Bullet3_aircraft_collide:function(){
        for(var j=0;j<30;j++){
            game.physics.arcade.overlap(this.boss1_bullet_3.bullets.getChildAt(j),this.aircraft,()=>{
                if(this.aircraft.blood>=20)this.aircraft.blood-=20;
                else this.aircraft.blood=0;
                var quake = game.add.tween(game.camera).to({x: game.camera.x - 10}, 100, Phaser.Easing.Bounce.InOut, false, 50, 3,true);
                quake.start();             
                this.boss1_bullet_3.bullets.getChildAt(j).kill();
                var explosion = this.explosions.getFirstExists(false);
                explosion.reset(this.aircraft.body.x+35, this.aircraft.body.y+30);
                explosion.play('boom', 30, false, true);
                if(this.aircraft.blood<=0){
                    this.aircraft.kill();
                    if(!this.airplane._exists)this.playerDie();
                }
                game.add.tween(this.aircraft).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                    game.add.tween(this.aircraft).to( {alpha:0 ,tint:0x220000}, 100, Phaser.Easing.Linear.None, true).yoyo(true,100).onComplete.add(()=>{
                        game.add.tween(this.aircraft).to( {alpha:0,tint:0x220000 }, 100, Phaser.Easing.Linear.None, true).yoyo(true,100)
                    });
                });
                canbeattackTime_ = game.time.now+1500;
                
            },null,this);
        }
    },
    setupInvader:function(invader) {

        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('boom');
    },
    setupInvader_:function(invader) {

        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('boom_big');

    }
};

var game = new Phaser.Game(650, 700, Phaser.AUTO, 'canvas');

var autofiringTimer = 0;
var firingTimer=0;
var modeTimer = 0;
var boss1_mode = 0;
var livingEnemies=[];
var targetEnemy;
var enemy1_Bullet;
var x=200;
var x_=200;
var volume = 300;
var canbeattackTime = 0;
var canbeattackTime_ = 0;
var bullet_type = 1;
var level = 0;
var create_enemy1_finish=false;
var create_boss1_finish=false;
var create_enemy2_finish=false;
var create_boss2_finish=false;
var BULLET;
var score = 0;





